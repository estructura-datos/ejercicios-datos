#include <stdio.h>

/*Ejercicios de datos, video 1*/

//Funci�n Invertir.
//Ingresa un n�mero entero.
//Sale el inverso del n�mero ingresado en la entrada.
int invertir(int num)
{
	int resultado, resultado2, invertido=0;
	while (num!=0)
	{
		resultado=num%10;
		resultado2=num/10;
		invertido=invertido*10+resultado;
		num=resultado2;
	}
	return invertido;
}

//Funci�n Sumatoria con While.
//Ingresa un n�mero entero.
//Sale la suma desde 0 hasta el n�mero ingresado.		
int sumatoriaWhile(int numero)
{
	int temp=0;
	int n=0;
	while (n<=numero)
	{
		temp=temp+n;
		n=n+1;
	}
	return temp;
	
}

//Funci�n Sumatoria con For.
//Ingresa un n�mero entero.
//Sale la suma desde 0 hasta el n�mero ingresado.
int sumatoriaFor(int numero)
{
	int i, suma=0;
	for (i=1; i<=numero; i++)
			suma += i;
	return suma;
	
}

//Funci�n Fibonacci.
//Ingresa un n�mero entero.
//Sale el fibonacci del n�mero ingresado.
int fibonacci (int numero)
{
  if (numero == 0){
        return 0;
    }
  else if (numero == 1){
        return 1;
    }
  else{
        return fibonacci(numero-1) + fibonacci(numero-2);
    }
}

//Funci�n Factorial.
//Ingresa un n�mero entero.
//Sale el factorial del n�mero ingresado.
int factorial (int numero)
{
  if (numero <= 1){
        return 1;
    }
  else{
        return numero * factorial (numero - 1);
    }

}
//Creaci�n de men� para manipular las funciones por medio de opciones a elegir.
//Al seleccionar la opci�n, se le pedir� que ingrese n�mero el cual se le aplicar� la funci�n.
//Cuando se muestra el resultado, volver� al men� nuevamente.
int main()
{
    //Men� Principal
    printf("\n Menu de Funciones\n");
    printf("\n 1. Par e Impar\n");
    printf("\n 2. Fibonacci\n");
    printf("\n 3. Factorial\n");
    printf("\n 4. Largo de un numero (While)\n");
    printf("\n 5. Largo de un numero (For)\n");
    printf("\n 6. Sumatoria (While)\n");
    printf("\n 7. Sumatoria (For)\n");
    printf("\n 8. Invertir\n");
    printf("\n 9. Palindromo\n");
    printf("\n 10. Salir\n");
    printf("\n Ingrese la opci�n: ");
    int op=0; //Variable de opci�n.
    scanf("%d",&op);

    if (op==1)
    {
		//Funci�n Par e Impar
		//Ingrese un n�mero entero.
		//En la salda dir� si es par o es impar.
        printf("\nEscriba un n�mero:");
        int numero;
        scanf("%d", &numero);
        if (numero % 2 == 0)
        {
            printf("\nEs par");
        }
        else
        {
            printf("\nEs impar");
        }
        return main();
    }

    if (op==2)
    {
        printf("\nEscriba un n�mero: ");
        int numero;
        scanf("%d", &numero);
        int resultado= fibonacci(numero);
        printf("\n El fibonacci es: %d \n", resultado);

        return main();
    }
    if (op==3)
    {
        printf("\n Escriba un n�mero: ");
        int numero;
        scanf("%d", &numero);
        int resultado= factorial(numero);
        printf("\n El factorial es: %d \n", resultado);

        return main();

    }
    if (op==4)
    {
		//Funci�n Largo de un n�mero con While
		//Ingrese un n�mero entero.
		//En la salda dir� cuantos digitos posee el n�mero ingresado.
        printf("\n Escriba un n�mero: ");
        int num, cont=1;
        scanf("%d",&num);

        while(num/10>0)
        {
            num=num/10;
            cont++;
        }
        printf("El largo es: %d \n",cont);
        return main();

    }
    if (op==5)
    {
		//Funci�n Largo de un n�mero con For.
		//Ingrese un n�mero entero.
		//En la salda dir� cuantos digitos posee el n�mero ingresado.
        printf("\n Escriba un n�mero: ");
        int num, cont=0;
        scanf("%d",&num);

		for (int i=1; num>0; num=num/10) 
        {
            cont++;
            i++;
        }
        printf("El largo es: %d \n",cont);
        return main();

    }
    if (op==6)
    {
        
        printf("\n Escriba un n�mero: ");
        int numero;
        scanf("%d", &numero);
        int resultado= sumatoriaWhile(numero);
        printf("\n La sumatoria es: %d \n", resultado);

        return main();
    }
    if (op==7)
    {
        
        printf("\n Escriba un n�mero: ");
        int numero;
        scanf("%d", &numero);
        int resultado= sumatoriaFor(numero);
        printf("\n La sumatoria es: %d \n", resultado);

        return main();
    }
    if (op==8)
    {
		
        printf("\n Escriba un n�mero: ");
        int numero;
        scanf("%d", &numero);
        int resultado= invertir(numero);
        printf("\n El inverso es: %d \n", resultado);

        return main();
    }
    if (op==9)
    {
		//Funci�n Palindromo.
		//Ingrese un n�mero entero.
		//En la salda dir� si es palindromo o no lo es.
        printf("\n Escriba un n�mero: ");
        int num;
        scanf("%d", &num);
		
		int resultado, resultado2, invertido=0;
		int temp=0;
		temp=num;
		while (num!=0)
		{
			resultado=num%10;
			resultado2=num/10;
			invertido=invertido*10+resultado;
			num=resultado2;
		}
		if (temp==invertido)
		{
			printf("Es palindromo\n");
		}
		else
		{
			printf("No es palindromo\n");
		}
		return main();
	
	}
	if (op==10)
    {
		//Opci�n para salir del men�.
        printf("\nHa salido del men�\n");
        return 0;
    }
     

}
