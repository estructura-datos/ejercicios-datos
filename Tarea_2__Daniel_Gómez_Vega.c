#include <stdio.h>
#include <stdlib.h>
#include <string.h>


/* Tarea 2, Estructura de datos
 * Daniel G�mez Vega / carn�: 2020207725*/

//Funci�n Eliminar Caracter.
//Ingresa un arreglo como par�metro de entrada.
//Se elimina el caracter elegido dentro del texto del arreglo.
//Retorna el nuevo texto con los cambios correspondientes.

char* eliminar_caracter(char* string)
{
	int contador=0;
	int contador_2=0;
	int  largo=0;
	largo=strlen(string);
	char* NewString=calloc(largo,sizeof(char));
	
	while (string[contador]!='\0')
	{
		if (string[contador]!='c')
		{
			NewString[contador_2]=string[contador];
			contador_2++;
		}
		contador++;
	}
	return (NewString);
}

//Invertir Cadena de Caracteres.
//Ingresa un arreglo como par�metro de entrada.
//Retorna el inverso de la cadena de caracteres.

char* invertir_cadena (char* string)
{
	int contador=0;
	int largo=0;
	largo=strlen(string);
	char* NewString=calloc(largo,sizeof(char));
	
	for (int x=largo-1; x>=0; x--)
	{
		NewString[contador]=string[x];
		contador++;
	}
	return (NewString);
}

// Literal a Heap.
//Ingresa un arreglo como par�metro de entrada.
//Retorna la variable de texto dento del heap.

char* literal_a_heap (char* string)
{
	int largo=0;
	largo=strlen(string);
	char* NewString=calloc(largo,sizeof(char));
	
	for (int i=0; i<=largo-1; i++)
	{
		NewString[i]=string[i];
	}
	return (NewString);
}

//Men� de Opciones.
//Se ingresa la opci�n que se desea realizar.
//Cuando se seleccion� esta retorna a la funci�n elegida.

int main()
{
	int op=0;
	printf("\nMen� de Funciones\n");
	printf("\n1. Eliminar Caracter ('c')\n");
	printf("2. Invertir Cadena de Caracteres\n");
	printf("3. De Literal a Heap\n");
	printf("4. Salir\n");
	printf("\nIngrese la opci�n:");
	scanf("%d",&op);
	
	if (op==1)
	{
		char* string="Hocla cMundcoccc";
		printf("\nTexto anterior:");
		puts(string);
		char* resultado=eliminar_caracter(string);
		printf("Texto Nuevo:");
		puts(resultado);
		printf("\nRegresar� al men� nuevamente\n");
		return main();
	}
	else if (op==2)
	{
		char* string="Estructura Datos";
		printf("\nTexto anterior:");
		puts(string);
		char* resultado=invertir_cadena(string);
		printf("Texto Nuevo:");
		puts(resultado);
		printf("\nRegresar� al men� nuevamente\n");
		return main();
	}
	else if (op==3)
	{
		char* string="Daniel";
		printf("\nTexto anterior:");
		puts(string);
		char* resultado=literal_a_heap(string);
		printf("Texto Nuevo:");
		puts(resultado);
		printf("\nRegresar� al men� nuevamente\n");
		return main();
	}
	else if (op==4)
	{
		printf("\nHa salido del men�");
		return 0;
	}
	else
	{
		printf("\nIngres� una opci�n no existente\n");
		printf("\nRegresar� al men� nuevamente\n");
		return main();
	}
}
